<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Devise extends Model
{
    public function changes()
    {
        return $this->hasMany('App\Change' , 'devise_id' , 'id');
    }
}
