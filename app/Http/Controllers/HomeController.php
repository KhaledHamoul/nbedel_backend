<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Devise;
use App\Change;

class HomeController extends Controller
{
    //
    function getLastChanges(){
        $devisesWithLastChange = Devise::with(['changes' => function ($query) {
            $query->distinct('devise_id')->orderBy('devise_id', 'desc')->latest()->limit(count(Devise::all()));
        }])->get();

        return response()->json(['devises' => $devisesWithLastChange , 'share_link' => 'https://nbedel.com', 'share_dialog_title' => 'Partagez avec vos amis' , 'share_title' => 'Nbedel.com'])->header('Cache-Control','no-cache');
    }

    function getAllChanges(){
        $deviseWithAllChanges = Devise::with(['changes' => function ($query) {
            $query->latest();
        }])->get();

        return response()->json(['devises' => $deviseWithAllChanges])->header('Cache-Control','no-cache');
    }
}
