<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('/test', function () {
    return response()
    ->json([
        'status' => 2019,
        'data' => 'boutef'
    ])
    ->header('Cache-Control','no-cache');
    
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/privacy', function () {
    return view('privacy');
});
