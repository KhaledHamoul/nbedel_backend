<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nbedel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>
<body>
    <div style="padding: 2% 5%;text-align: left">
    <h1 style="text-align: center">Nbedel Mobile Application</h1>
    <h2>PRIVACY POLICY</h2>
<b>Effective Date:</b> April 25th, 2019 <br>
Welcome to Nbedel. Your privacy is important to us. <br>
<h4>This Privacy Policy describes: </h4>
<u>
    <li>The ways we collect personal data about you and why we do so.</li>
    <li>How we use your personal data.</li>
</u>
<br>
This Privacy Policy applies to Nbedel Mobile application. We may periodically update this Privacy Policy by posting a new version on nbedel.com. If we make any material changes, we will notify you by posting a notice in our website.<br><br>
If you have any questions, requests or concerns about data protection, please contact us with the information provided in the ‘Contact Information section of this Policy.<br>
<h2>The Data We Collect (automatically)</h2>
Your mobile device identifiers (advertising ID used by AdMob service).
<h2>Why Do We Collect Your Data</h2>
<h3>Mainely To show personalized advertisements</h3>
To show you personalized advertisements (more relevant advertisement instead of random advertisement, the amount remaining same in both cases) in the application we have a legitimate interest to process necessary data to:
<u>
<li>Track the content you access in connection with the Service and your online behavior</li>
<li>Deliver, target and improve our advertising and the Service</li>
</u><br>
<h2>Who Can See Your Data</h2>
Apart from Nbedel owner, your data can be accessed by Our advertising partner (AdMob) that may access your data and operate under its own <a href="https://policies.google.com/privacy">privacy policy</a>. We encourage you to check it to learn more about its data processing practices.
<h2>Age Limits</h2>
We do not knowingly collect or solicit personal data about or direct or target interest based advertising to anyone under the age of 13 or knowingly allow such persons to use our Services.  If you are under 13, please do not send any data about yourself to us.  If we learn that we have collected personal data about a child under age 13, we will delete that data.
<h2>Contact Information</h2>
Nbedel.com/contact<br>
Algiers, Algeria.<br>
<b>Email:</b> sidisaidkarim@gmail.com<br><br>
If you have questions about data protection, we encourage you to primarily contact us through our website nbedel.com or via the email provided with the app in google play store.

    </div>
</body>
</html>